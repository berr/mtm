import numpy as np
import matplotlib.pyplot as plt

#f = lambda x: 1.0 / (25 + x**2)
f = lambda x: 1.0 / (1 + 25* x**2)



def create_interpolating_polynomial(n):
    a = np.matrix(np.ones((n,n), dtype=np.float64), copy=False)
    xs = np.linspace(-1, 1, n).reshape((n, 1))
    a[:, 1] = xs
    for i in xrange(2, n):
        a[:, i] = np.multiply(a[:, i-1], xs)

    #
    #     [1    x0  x0^2 ... x0^n ]
    #     [1    x1  x1^2 ... x1^n ]
    # a = [1    x2  x2^2 ... x2^n ]
    #     [...  ... ...  ... ...  ]
    #     [1    xn  xn^2 ... xn^n ]

    b = f(xs)
    #     [ f(a0) ]
    #     [ f(a1) ]
    # b = [  ...  ]
    #     [f(an-1)]
    #     [ f(an) ]

    
    #     [ a0 ]
    #     [ a1 ]
    # x = [ .. ]
    #     [an-1]
    #     [ an ]

    # x is the coefficient vector for the canonical basis.
    return np.linalg.solve(a, b)



whole_interval = np.linspace(-1,1, 2000)
negative_interval = np.linspace(-1, -0.726, 2750)
center_interval = np.linspace(-0.726, 0.726, 1453)
positive_interval = np.linspace(0.726, 1, 2750)


ns = [5, 10, 20, 30, 40, 50, 100, 200, 300, 400, 500, 1000]
for n in ns:    
    xs = np.linspace(-1,1, n)
    pn = np.array(list(reversed(create_interpolating_polynomial(n))))

    f_negative_interval = f(negative_interval)
    f_center_interval = f(center_interval)
    f_positive_interval = f(positive_interval)
    #f_whole_interval = f(whole_interval)

    pn_negative_interval = np.polyval(pn, negative_interval)
    pn_center_interval = np.polyval(pn, center_interval)
    pn_positive_interval = np.polyval(pn, positive_interval)
    #pn_whole_interval = np.polyval(pn, whole_interval)
    
    max_index_negative_interval = np.max(np.abs(f_negative_interval - pn_negative_interval))    
    max_index_center_interval = np.max(np.abs(f_center_interval - pn_center_interval))
    max_index_positive_interval = np.max(np.abs(f_positive_interval - pn_positive_interval))
    

    
    #print max_index_negative_interval
    #print max_index_center_interval
    #print max_index_positive_interval

    print "{} & {:g} & {:g} & {:g} \\\\".format(n, max_index_negative_interval, max_index_center_interval, max_index_positive_interval)

    #plt.plot(positive_interval, f_positive_interval, 'k-', label='$f$')
    #plt.plot(positive_interval, pn_positive_interval, 'k--', label='$P_{' + str(n) + '}$')

    #plt.plot(whole_interval, f_whole_interval - pn_whole_interval, 'k-', label='$f - P_{' + str(n) + '}$')


    #plt.legend()
    #plt.grid()
    #plt.show()
    

    print ''


    
    
    





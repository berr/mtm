import numpy as np
import matplotlib.pyplot as plt

f = lambda x: x**4
pn = lambda x: 2*x**3 + x*x - 2*x

x = np.linspace(-1,2, 1000)
fy = np.array([f(xx) for xx in x])
pny = np.array([pn(xx) for xx in x])


plt.plot(x, fy, 'k-', label='$f$')
plt.plot(x, pny, 'k--', label='$P_3(x)$')
plt.legend()
plt.grid()


max_index = np.argmax(np.abs(fy - pny))
print x[max_index], fy[max_index], pny[max_index]

#plt.show()



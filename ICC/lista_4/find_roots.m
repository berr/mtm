function roots = find_roots (polynomial)
  % [1 2 0 3] = x^3 + 2x^2 + 0x + 3  
  
  if size(size(polynomial, 1) == 1)
    polynomial = transpose(polynomial);
  end
  
  degree = size(polynomial, 1);
  n_roots = degree - 1;
  roots = zeros(n_roots, 1);

  function x0 = initial_value (p)
    m = max(abs(p(2:size(p))));
    s = sign(p(1));
    x0 = s * m * size(p,1) / p(1);
    x0 = max([1 x0]);  
  end

  
  function r = evaluate_horner(p, x)
    r = p(1);
    for i=2:size(p)
      r = r * x + p(i);
    end
  
  end
  
  
  function d = derive_polynomial(p)
    d = p(1:size(p)-1);
    factors_to_derive = linspace(size(p,1) - 1, 1, size(p,1) - 1)';
    d = d .* factors_to_derive;  
  end
  
  
  function r = newton_method(p, pd, initial)
    r = initial
    fr = evaluate_horner(p, r);
    while (abs(fr) > precision)
      r = r - fr / evaluate_horner(pd, r);
      fr = evaluate_horner(p, r);
    end  
  end
  
  
  function n = deflate_polynomial(p, root)
    n = zeros(size(p, 1) - 1, 1);
    n(1) = p(1);
    for k=2:size(p) - 1
      n(k) = n(k-1) * root + p(k);
    end
  
  end
    
    
  precision = 1e-10;  
  current_polynomial = polynomial;  
   
  for j=1:n_roots
    if j==1
      x0 = initial_value(polynomial);
    else
      x0 = roots(n_roots - j + 2);
    end
        
    
    current_derivative = derive_polynomial(current_polynomial);
    x0 = newton_method(current_polynomial, current_derivative, x0);        
    roots(n_roots - j + 1) = x0;    
    
    current_polynomial = deflate_polynomial(current_polynomial, x0);
  
  end

  
  improved_roots = roots;  
  original_derivative = derive_polynomial(polynomial);
  
  for j=1:n_roots
    x0 = roots(n_roots - j + 1);    
    x0 = newton_method(polynomial, original_derivative, x0);        
    improved_roots(n_roots - j + 1) = x0;    
  end
  
  
  for l=1:n_roots
    original = abs(evaluate_horner(polynomial, roots(l)));
    improved = abs(evaluate_horner(polynomial, improved_roots(l)));  
    disp(sprintf('$z_%d$ & %0.15g & %0.15g & %0.15g & %0.15g \\\\', l, roots(l), original, improved_roots(l), improved));
  end
  
  roots = improved_roots;  
  
end
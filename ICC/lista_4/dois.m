function dois(x0)

function r = f(x)
  r = (log(2) / pi) * sin((2*pi/log(2)) * log(x)) + 1;
end

function r = ff(x)
  r = 2* cos((2*pi/log(2)) * log(x)) / x;
end

for i=0:100
  x1 = x0 - f(x0) / ff(x0);
  if i == 1 | i == 5 | mod(i, 10) == 0
    disp(i)
    disp(x1)
  end
  x0 = x1;  
end;

end


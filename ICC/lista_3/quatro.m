for i=0:13

  x = 10^(-i);

  result = sinh(x)


  taylor_approximation = x + x^3/factorial(3) + x^5/(factorial(5)) + x^7/(factorial(7))      
  taylor_absolute_error = abs(taylor_approximation - result);
  taylor_relative_error = taylor_absolute_error / result

  formula = (exp(x) - exp(-x))/2  
  formula_absolute_error = abs(formula - result);
  formula_relative_error = formula_absolute_error / result

  disp("")

end

#include <iostream>
#include <cmath>


template <class T>
T original(T x) {
  return std::sinh(x);
}

template <class T>
T f(T x) {  
  return (std::exp(x) - std::exp(-x)) / 2;
}

template <class T>
T taylor(T x) {
  T s = x;

  T d = 1;
  T n = x;
  
  for (int i = 3; i <= 7; i+=2) {
    n *= x * x;
    d *= (i - 1) * i;
    s += n/d;
  }
  
  return s;
}


int main() {

  double xd;
  float xf;
  
  for (int i = 0; i <= 13; ++i) {
    xd = std::pow(10, -i);
    xf = std::pow(10, -i);
    std::cout << "x = " << xd << std::endl;
    double o = original<double> (xd);
    double form = f<double> (xd);
    double tayl = taylor<double> (xd);
    
    std::cout << "double: original=" << o << " f=" << form << " taylor=" << tayl << std::endl;
    std::cout << "frel=" << (o - form) / o << std::endl;
    std::cout << "trel=" << (o - tayl) / o << std::endl;


    //std::cout << "float: original=" << original<float> (xf) << " f=" << f<float> (xf) << " taylor=" << taylor<float> (xf) << std::endl;
    std::cout << std::endl;    
  }

  return 0;  
}

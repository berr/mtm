#include <iostream>
#include <cmath>

double originald(double x) {
  return (1 - std::cos(x)) / std::cos(x);  
}

double changedd(double x) {
  return std::sin(x) / (1 + std::cos(x));  
}

float originalf(float x) {
  return (1 - std::cos(x)) / std::cos(x);  
}

float changedf(float x) {
  return std::sin(x) / (1 + std::cos(x));  
}


int main() {

  double xd;
  float xf;
  
  for (int i = 0; i < 10; ++i) {
    xd = std::pow(10, -i);
    xf = std::pow(10, -i);
    std::cout << "x = " << xd << std::endl;
    std::cout << "double: original=" << originald(xd) << " changed=" << changedd(xd) << std::endl;
    //std::cout << "float: original=" << originalf(xf) << " changed=" << changedf(xf) << std::endl;
    std::cout << std::endl;    
  }

  return 0;  
}

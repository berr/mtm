#include <iostream>
#include <cmath>

double originald(double x) {
  return std::sqrt(x + 1) -1;
}

double changedd(double x) {
  return x / (std::sqrt(x + 1) + 1);
}

float originalf(float x) {
  return std::sqrt(x + 1) -1;
}

float changedf(float x) {
  return x / (std::sqrt(x + 1) + 1);
}


int main() {

  double xd;
  float xf;
  
  for (int i = 0; i < 20; ++i) {
    xd = std::pow(10, -i);
    xf = std::pow(10, -i);
    std::cout << "x = " << xd << std::endl;
    std::cout << "double: original=" << originald(xd) << " changed=" << changedd(xd) << std::endl;
    std::cout << "float: original=" << originalf(xf) << " changed=" << changedf(xf) << std::endl;
    std::cout << std::endl;    
  }

  return 0;  
}

#include <iostream>
#include <cmath>

double y = 1.57079632679f;

template <class T>
T original(T x) {
  return std::sin(x) - std::sin(y);
}

template <class T>
T changed(T x) {
  T cx = std::cos(x);
  T sx = std::sin(x);

  T cy = std::cos(y);
  T sy = std::sin(y);
  
  return (1 - sy * sy - cx*cx) / (sx + sy);
  //return (cy * cy - cx*cx) / (sx + sy);
}


int main() {

  double xd;
  float xf;
  
  for (int i = 0; i < 20; ++i) {
    xd = y + std::pow(10, -i);
    xf = y + std::pow(10, -i);
    std::cout << "x = " << xd << std::endl;
    std::cout << "double: original=" << original<double> (xd) << " changed=" << changed<double> (xd) << std::endl;
    std::cout << "float: original=" << original<float> (xf) << " changed=" << changed<float> (xf) << std::endl;
    std::cout << std::endl;    
  }

  return 0;  
}

function comparison
  precision = 1e-6;

  function h = evaluate_horner(p, x)
    h = p(1);
    for i=2:size(p)
      h = h * x + p(i);
    end
  
  end
  
  
  function d = derive_polynomial(p)
    d = p(1:size(p)-1);
    factors_to_derive = linspace(size(p,1) - 1, 1, size(p,1) - 1)';
    d = d .* factors_to_derive;  
  end
  
  
  function r = newton_method(p, pd, initial)
    r = initial;
    fr = evaluate_horner(p, r);
    while (abs(fr) > precision)
      r = r - fr / evaluate_horner(pd, r);
      fr = evaluate_horner(p, r);
    end  
  end
  
  function a = secant_method(p, a, b)
    fa = evaluate_horner(p, a);
    fb = evaluate_horner(p, b);
    
    while (abs(fa) > precision)
      c = a - (fa*(a - b))/(fa - fb);
      
      b = a;
      fb = fa;
      a = c;
      fa = evaluate_horner(p, a);
    end
  
  end
  
  function c = false_position_method(polynomial, a, b)
    fa = evaluate_horner(polynomial, a);
    fb = evaluate_horner(polynomial, b);
    
    if (fa > 0)
      assert(fb < 0);
      p = a;
      fp = fa;
      
      n = b;
      fn = fb;
    else
      assert(fa < 0);
      assert(fb > 0);
      
      p = b;
      fp = fb;
      
      n = a;
      fn = fa;
    end
    
    
    fc = fa;
    while (abs(fc) > precision)
      c = p - (fp*(p - n))/(fp - fn);
      fc = evaluate_horner(polynomial, c);
      
      if(fc > 0)
        p = c;
        fp = fc;
      else
        n = c;
        fn = fc;
      end
      
    end
  
  end
     
  f = [230; 18; 9; -221; -9];
  df = derive_polynomial(f);
  
  
  newton_first = newton_method(f, df, -0.5);
  newton_first_error = evaluate_horner(f, newton_first);
  newton_second = newton_method(f, df, 0.5);
  newton_second_error = evaluate_horner(f, newton_second);
  
  
  disp('Newton');
  disp(sprintf('[-1,0] : x = %0.15g, error = %0.15g', newton_first, newton_first_error));
  disp(sprintf('[0,1] : x = %0.15g, error = %0.15g', newton_second, newton_second_error));
  
  disp(sprintf('\n'));
  
  secant_first = secant_method(f, -1, 0);
  secant_first_error = evaluate_horner(f, secant_first);
  secant_second = secant_method(f, 0, 1);
  secant_second_error = evaluate_horner(f, secant_second);
  
  
  disp('Secant');
  disp(sprintf('[-1,0] : x = %0.15g, error = %0.15g', secant_first, secant_first_error));
  disp(sprintf('[0,1] : x = %0.15g, error = %0.15g', secant_second, secant_second_error));
  
  disp(sprintf('\n'));
  
  false_position_first = false_position_method(f, -1, 0);
  false_position_first_error = evaluate_horner(f, false_position_first);
  false_position_second = false_position_method(f, 0, 1);
  false_position_second_error = evaluate_horner(f, false_position_second);
  
  
  disp('false_position');
  disp(sprintf('[-1,0] : x = %0.15g, error = %0.15g', false_position_first, false_position_first_error));
  disp(sprintf('[0,1] : x = %0.15g, error = %0.15g', false_position_second, false_position_second_error));
  
  disp(sprintf('\n'));
  
  
  %x = linspace(-1,1, 100);
  %y = polyval(f, x);
  
  %plot(x,y);
  
  

end
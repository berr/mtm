import numpy as np

def erf_partial_sum(sum_index, x):
    numerator = np.power(x, 2 * sum_index + 1)
    denominator = (2 * sum_index + 1)  * np.math.factorial(sum_index)
    return numerator / denominator
    
def erf(x, error):
    current_error = error + 1

    last_value = erf_partial_sum(0, x)
    current_sum = last_value
    k = 0
    signal = 1

    while current_error > error:
        k += 1
        signal = -1 * signal
        
        current_value = signal * erf_partial_sum(k, x)
        current_sum += current_value
        current_error = np.abs(current_value - last_value)


        last_value = current_value

    result = 2 / np.sqrt(np.pi) * current_sum
        
    print("sum: {}\nk={}\nerror={}".format(result, k, current_error))


erf(1, 1e-6)        



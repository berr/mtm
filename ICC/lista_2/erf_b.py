import numpy as np


def erf_partial_sum(sum_index, x):
    numerator = np.power(x, 2 * sum_index + 1)
    denominator = (2 * sum_index + 1)  * np.math.factorial(sum_index)
    return numerator / denominator


def erf(x, k):

    total_sum = 0
    for i in range(k + 1):
        numerator = np.power(2, i) * np.power(x, 2 * i + 1)
        denominator = np.product(range(1, 2*i + 2, 2))
        total_sum += numerator / denominator

    
    result = 2 / np.sqrt(np.pi) / np.e * total_sum
        
    print("sum: {}".format(result))


erf(1, 10)



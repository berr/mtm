import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.rcParams['text.usetex']=True



x = np.linspace(-10, 10, 10000)

original = np.cos(x)
t2 = 1 - (x**2) / 2
t4 = 1 - (x**2) / 2 + (x**4) / 24
t6 = 1 - (x**2) / 2 + (x**4) / 24 - (x**6) / 720
t8 = 1 - (x**2) / 2 + (x**4) / 24 - (x**6) / 720 + (x**8) / 40320


f = plt.figure()

#plt.plot(x, original, label='cos(x)', linestyle='-')
plt.plot(x, np.abs(original - t2), label=r'$|cos(x) - (1 -\frac{x^2}{2!})|$', linestyle='--')
plt.plot(x, np.abs(original - t4), label=r'$|cos(x) - (1 -\frac{x^2}{2!} + \frac{x^4}{4!})|$', linestyle='-.')
plt.plot(x, np.abs(original - t6), label=r'$|cos(x) - (1 -\frac{x^2}{2!} + \frac{x^4}{4!} - \frac{x^6}{6!})|$', linestyle=':')
plt.plot(x, np.abs(original - t8), label=r'$|cos(x) - (1 -\frac{x^2}{2!} + \frac{x^4}{4!} - \frac{x^6}{6!} + \frac{x^8}{8!})|$', linestyle='-')



plt.xlim(-5, 5)
plt.ylim(0, 1.4)
plt.legend(loc="best")


plt.show()


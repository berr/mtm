import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.rcParams['text.usetex']=True



x = np.linspace(np.pi, np.pi, 1000)

cos = np.cos(x)
approximation = -x**2 / 2 + 1
error = np.abs(cos - approximation)


f = plt.figure()
plt.plot(x, error, label=r"$|cos(x) - (\frac{-x^2}{2}+1)|$", linestyle='solid')
plt.plot([-10,10], [0.1,0.1], "k--")

error_boundaries = 1.2611614072916721
plt.plot([-error_boundaries, -error_boundaries], [-1, 1], "k--")
plt.plot([error_boundaries, error_boundaries], [-1, 1], "k--")


plt.xlim(-1.5, 1.5)
plt.ylim(-0.1, 0.5)
plt.xticks(list(plt.xticks()[0]) + [-1.26, 1.26])
plt.legend(loc="best")




plt.show()


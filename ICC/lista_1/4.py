import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.rcParams['text.usetex']=True



x = np.linspace(-3, 3, 1000)

quadratic = 2 + (x-1)/4 - ((x-1)**2) / 64
linear = (x + 7) / 4
original = np.sqrt(x+3)


f = plt.figure()

plt.plot(x, np.abs(original-linear), label=r'$|\sqrt{x+3} - \frac{x+7}{4}|$', linestyle='dashed')
plt.plot(x, np.abs(original-quadratic), label=r'$|\sqrt{x+3} - (2 + \frac{1}{4}(x-1) - \frac{1}{64}(x-1)^2)|$')


plt.xlim(0, 2)
plt.ylim(0, 0.02)
plt.legend(loc="best")




plt.show()


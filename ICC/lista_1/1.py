import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.rcParams['text.usetex']=True



x = np.linspace(-2 * np.pi, 2*np.pi, 1000)

cos = np.cos(x)
linear = np.ones(1000)
approximation = -x**2 / 2 + 1


f = plt.figure()
plt.plot(x, cos, label="cos(x)", linestyle='solid')
plt.plot(x, linear, label="1", linestyle='dashed')
plt.plot(x, approximation, label=r"$\frac{-x^2}{2}+1$", linestyle='dashdot')
plt.xlim(-4,4)
plt.ylim(-2, 2)
plt.legend(loc="lower center")


plt.show()


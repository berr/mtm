import numpy as np

def newton_method(f, f_prime, x0, precision=1e-7, max_iter=200):
    current_x = float(x0)
    current_y = f(current_x)
    iters = 0
    error = precision + 1

    results = [(current_x, current_y)]
    
    while error > precision and iters < max_iter:
        next_x = current_x - current_y / f_prime(current_x)
        error = abs(next_x - current_x)
        current_x = next_x

        current_y = f(current_x)
        
        iters += 1
        results.append((current_x, current_y))

    if abs(current_y) > precision:
        raise RuntimeError('Did not converge in ' + str(iters) + ' iterations')

    return results

def halley_method(f, f_prime, f_double_prime, x0, precision=1e-7, max_iter=200):
    current_x = float(x0)
    current_y = f(current_x)
    iters = 0

    error = precision + 1

    results = [(current_x, current_y)]
    while error > precision and iters < max_iter:
        current_y_prime = f_prime(current_x)
        numerator = 2 * current_y * current_y_prime
        denominator = current_y * f_double_prime(current_x) - 2 * current_y_prime * current_y_prime
        next_x = current_x + numerator / denominator

        error = abs(next_x - current_x)
        current_x = next_x

        current_y = f(current_x)        
        iters += 1
        results.append((current_x, current_y))

    if abs(current_y) > precision:
        raise RuntimeError('Did not converge in ' + str(iters) + ' iterations')

    return results
    


def test_function(function, derivative, second_derivative, initial, precision=1e-7, max_iter=200):
    newton_results = newton_method(function, derivative, initial, precision=precision, max_iter=max_iter)
    newton_xs = [r[0] for r in newton_results]
    newton_fxs = [r[1] for r in newton_results]

    halley_results = halley_method(function, derivative, second_derivative, initial, precision=precision, max_iter=max_iter)
    halley_xs = [r[0] for r in halley_results]
    halley_fxs = [r[1] for r in halley_results]

    best_result = halley_method(function, derivative, second_derivative, initial, precision=1e-15, max_iter=5000)
    best_x = best_result[-1][0]

    format_float = lambda x: '{:.6g}'.format(x)
    
    print 'best: x={:.10g}, f(x)={:.6g}'.format(best_x, function(best_x))


    newton_x_differences = [np.abs(x - best_x) for x in newton_xs]
    halley_x_differences = [np.abs(x - best_x) for x in halley_xs]
    

    to_string = lambda x: [format_float(d) for d in x]
    newton_xs = to_string(newton_xs)
    newton_x_differences = to_string(newton_x_differences)
    newton_fxs = to_string(newton_fxs)

    halley_xs = to_string(halley_xs)
    halley_x_differences = to_string(halley_x_differences)
    halley_fxs = to_string(halley_fxs)                           

    if len(newton_xs) > len(halley_xs):
        extension = [''] * (len(newton_xs) - len(halley_xs))
        halley_xs.extend(extension)
        halley_fxs.extend(extension)
        halley_x_differences.extend(extension)
    else:
        extension = [''] * (len(halley_xs) - len(newton_xs))
        newton_xs.extend(extension)
        newton_fxs.extend(extension)
        newton_x_differences.extend(extension)        
    
    for i,r in enumerate(zip(newton_xs, newton_x_differences, newton_fxs, halley_xs, halley_x_differences, halley_fxs)):
        print ' & '.join((str(i+1),) + r), ' \\\\'

    print '\n', '=' * 80, '\n', '='*80


f = lambda x: x* x - 4
f_prime = lambda x: 2 * x
f_double_prime = lambda x: 2
#test_function(f, f_prime, f_double_prime, initial=0.1)
        

g = lambda x: np.cos(x) - x
g_prime = lambda x: -np.sin(x) - 1
g_double_prime = lambda x: -np.cos(x)
test_function(g, g_prime, g_double_prime, initial=np.pi)


h = lambda x: x * x - 231
h_prime = lambda x: 2 * x
h_double_prime = lambda x: 2
test_function(h, h_prime, h_double_prime, initial=231)


i = lambda x: (48*x -1)*(1+x)**60 + 1
i_prime = lambda x: 48*(x + 1) ** 60 + 60 * (48*x - 1) * (x + 1)**59
i_double_prime = lambda x: 48 * (x+1) ** 60 + 60 * (48*x-1) * (x+1) ** 59

initial_random = (np.random.rand(1) - 0.5) * 10
#test_function(i, i_prime, i_double_prime, initial=initial_random, precision=1e-15, max_iter=1000)


j = lambda x: (5*x**3 + 6*x -1) ** 8
j_prime = lambda x: 24 * (5* x ** 2 + 2) *  (5*x**3 + 6*x - 1)**7
j_double_prime = lambda x: 24 * (5 * x**3 + 6*x-1)**6 * (575 * x**4 + 480*x**2 -10*x + 84)

initial_random = (np.random.rand(1) - 0.5) * 10
test_function(j, j_prime, j_double_prime, initial=0.163, precision=1e-7, max_iter=1000)


from least_squares import least_squares
import numpy as np
from matplotlib import pyplot as plt

def evaluate_polynomial(p, x):
    '''
    Evaluate the given polynomial at the given point.

    p = [a0, a1, ..., an] = a0 + a1 x + a2x^2 + ... + anx^n
    '''
    value = 0
    for a in reversed(p):
        value = value * x + a

    return value


def show_least_squares(x, y, degree):
    plt.figure()

    x_min = np.min(x) - 1
    x_max = np.max(x) + 1
    y_min = np.min(y) - 1
    y_max = np.max(y) + 1

    plt.plot(x, y, 'bo')
    plt.xlim(x_min, x_max)
    plt.ylim(y_min, y_max)

    approximation = least_squares(x, y, degree)

    p_domain = np.linspace(x_min, x_max)
    p_image = [evaluate_polynomial(approximation, point) for point in p_domain]

    plt.plot(p_domain, p_image, 'k--')



def generate_polynomial_with_noise(degree):
    coefficients = np.random.randint(-5, 6, degree + 1)
    domain = np.linspace(-5, 5)
    domain += np.random.normal(0, 1, len(domain)) # add some noise
    domain.sort() # keep domain ordered

    image = [evaluate_polynomial(coefficients, x) for x in domain]
    image += np.random.normal(1, 7, len(image))

    return domain, image



#show_least_squares([1, 2, 3], [1, 3, 1], degree=1)
#show_least_squares([0.1, 0.2, 0.3, 0.4, 0.5], [-0.18, 0.31, 1.03, 2.48, 3.73], degree=2)
#show_least_squares([0, 1, 2, 3], [1, 3, 4, 4], degree=2)
show_least_squares(*generate_polynomial_with_noise(2), degree=2)
show_least_squares(*generate_polynomial_with_noise(3), degree=3)

plt.show()

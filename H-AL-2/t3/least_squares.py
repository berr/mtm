import numpy as np
import numpy.linalg


def least_squares(x, y, degree=1):
    '''
    Returns the coefficients for the polynomial of degree `degree` that has the
    '''
    a = np.ones((len(x), degree + 1))
    for i in xrange(1, degree + 1):
        a[:,i] = np.multiply(a[:, i- 1], x)

    a = np.matrix(a, copy=False, dtype=np.float64)

    at = np.transpose(a)
    b = np.matrix(y, dtype=np.float64, copy=False).reshape((-1, 1))

    result = numpy.linalg.solve(at * a, at * b)

    #return as an uni-dimensional array
    return result.getA().reshape(-1)

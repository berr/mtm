from numpy.testing import assert_array_almost_equal, assert_almost_equal
from least_squares import least_squares



def test_determined_system():
    obtained, error = least_squares([0, 1], [0, 1], degree=1)
    expected = [1, 0]

    assert_array_almost_equal(obtained, expected)
    assert_almost_equal(error, 0)


def test_linear_curve():
    '''
    Example from Elementary linear algebra, Howard Anton / Chris Rorres, 10th edition
    '''
    x = [0, 1, 2, 3]
    y = [1, 3, 4, 4]

    obtained, error = least_squares(x, y, degree=1)

    expected = [1, 1.5]
    assert_array_almost_equal(obtained, expected)


def test_quadratic_curve():
    '''
    Example from Elementary linear algebra, Howard Anton / Chris Rorres, 10th edition
    '''
    x = [0.1, 0.2, 0.3, 0.4, 0.5]
    y = [-0.18, 0.31, 1.03, 2.48, 3.73]

    obtained, error = least_squares(x, y, degree=2)

    expected = [16.071429, 0.347143, -0.398]
    assert_array_almost_equal(obtained, expected)


def test_exact_cubic_curve():
    cubic = lambda x: x**3 - x**2 - x + 20
    x = [0.1, 0.2, 0.3, 0.4, 0.5]
    y = [cubic(d) for d in x]

    obtained, error = least_squares(x, y, degree=3)

    expected = [1, -1, -1, 20]
    assert_array_almost_equal(obtained, expected)
    assert_almost_equal(error, 0)
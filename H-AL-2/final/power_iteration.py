import numpy as np
from numpy.linalg import norm


def power_iteration(a, initial, max_iterations=500):
    a = np.asmatrix(a, dtype=np.float64)
    initial = np.asmatrix(initial).reshape((-1, 1))

    current_vector = initial
    current_vector_norm = norm(current_vector)

    next_vector = a * current_vector
    next_vector_norm = norm(next_vector)

    error = norm(next_vector - current_vector_norm * current_vector)

    next_vector = next_vector / next_vector_norm

    total_iterations = 0
    while error > 1e-7 and total_iterations < max_iterations:
        current_vector = next_vector
        current_vector_norm = next_vector_norm

        next_vector = a * current_vector
        next_vector_norm = norm(next_vector)

        error = norm(next_vector - current_vector_norm * current_vector)
        next_vector /= next_vector_norm

        total_iterations += 1

    if total_iterations == max_iterations:
        raise RuntimeError("Iteration did not converge")

    next_vector = a * next_vector
    value = norm(next_vector)
    vector = next_vector / value

    return value, vector






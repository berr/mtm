import numpy as np
import pytest
from numpy.testing import assert_array_almost_equal, assert_almost_equal
from numpy.linalg import norm
from numpy import inner
from qr_decomposition import  qr_decomposition



def test_identity():
    a = np.identity(3)

    q, r = qr_decomposition(a)

    expected_q = np.identity(3)
    expected_r = np.identity(3)

    assert_array_almost_equal(q, expected_q)
    assert_array_almost_equal(r, expected_r)


def test_rectangular_matrix():
    a = [[9, 0, 26], [12, 0, -7], [0, 4, 4], [0, -3, -3]]

    q, r = qr_decomposition(a)

    expected_q = [[0.6, 0, 0.8], [0.8, 0, -0.6], [0, 0.8, 0], [0, -0.6, 0]]
    expected_r = [[15, 0, 10], [0, 5, 5], [0, 0, 25]]

    assert_array_almost_equal(q, expected_q)
    assert_array_almost_equal(r, expected_r)


def test_square_matrix():
    a = np.matrix([[3, -2, -10], [10, 8, 8], [-4, 9, -2]])

    q, r = qr_decomposition(a)

    a1 = q[:, 0].getA1()
    a2 = q[:, 1].getA1()
    a3 = q[:, 2].getA1()

    # Columns should be orthogonal
    assert_almost_equal(inner(a1, a2), 0)
    assert_almost_equal(inner(a1, a3), 0)
    assert_almost_equal(inner(a2, a3), 0)

    # Columns should be unitary
    assert_almost_equal(norm(a1), 1.0)
    assert_almost_equal(norm(a2), 1.0)
    assert_almost_equal(norm(a3), 1.0)


    assert_array_almost_equal(a, q * r)


def test_other_rectangular_matrix():
    a = np.matrix([[1, 2], [1, 1], [2, 3], [2, 2]])

    q, r = qr_decomposition(a)

    a1 = q[:, 0].getA1()
    a2 = q[:, 1].getA1()

    # Columns should be orthogonal
    assert_almost_equal(inner(a1, a2), 0)


    # Columns should be unitary
    assert_almost_equal(norm(a1), 1.0)
    assert_almost_equal(norm(a2), 1.0)

    assert_array_almost_equal(a, q * r)


import numpy as np
from numpy.linalg import norm

def qr_decomposition(a):
    a = np.matrix(a, copy=False, dtype=np.float64)

    rows, columns = a.shape
    if columns > rows:
        raise RuntimeError("More rows than columns. Columns aren't linear independent.")


    q = a.copy()

    q[:, 0] = q[:, 0] / (norm(q[:, 0]))

    # Create Q as an orthonormal basis for span(columns of a)
    for i in xrange(1, a.shape[1]):
        for j in xrange(i):
            q_j = q[:, j]
            projection = (q_j * q_j.transpose() / (float(q_j.transpose() * q_j))) * a[:,i]
            q[:, i] = q[:, i] - projection

        q[:, i] = q[:, i] / (norm(q[:, i]))



    r = np.matrix(np.zeros((columns, columns)))

    #     [<q1, a1> <q1, a2> ... <q1, an>]
    # r = [0        <q2, a2> ... <q2, an>]
    #     [           ...                ]
    #     [0        0        ... <qn, an>]


    for i in xrange(columns):
        q_i_t = q[:, i].transpose()
        for j in xrange(i, columns):
            r[i,j] = float(q_i_t * a[:,j])


    return q, r

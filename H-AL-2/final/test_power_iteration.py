import numpy as np
import pytest
from power_iteration import power_iteration
from numpy.testing import assert_almost_equal



@pytest.mark.parametrize("initial", [(1,1), (1, 0), (0, 1), (1000, 2000), (-666, 3)])
def test_simple_matrix(initial):
    a = [[4, 1], [6, 3]]

    # eigenvalues: 6, 1
    # eigenvectors: (1,2), (1,-3), respectively

    value, vector = power_iteration(a, initial)
    expected_value = 6

    assert_almost_equal(value, expected_value)


@pytest.mark.parametrize("initial", [
    (1,1,1,1,1),
    (1,0,0,0,0),
    (0,1,0,0,0),
    (0,0,1,0,0),
    (0,0,0,1,0),
    (0,0,0,0,1),
])
def test_matrix_a(initial):
    a = np.zeros((5,5))

    a[0,0] = 3
    a[0,1] = 0.5
    a[0,3] = -0.5
    a[0,4] = -np.sqrt(2)

    a[1,1] = 3
    a[1,2] = 1.5

    a[2,2] = 3
    a[2,3] = -1.0/6
    a[2,4] = -np.sqrt(2)/3

    a[3,3] = 3

    a[4,4] = 3

    for i in xrange(1, 5):
        for j in xrange(i):
            a[i,j] = a[j,i]

    # eigenvalues: 1, 2, 3, 4, 5

    value, vector = power_iteration(a, initial)
    expected_value = 5

    assert_almost_equal(value, expected_value)


@pytest.mark.parametrize("initial", [
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [-10, 8, 8, 8, -1, -1, 3, 4, 7, 7],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1], # does not converge
    [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1], # does not converge
    [-7, -4, -6, -3, 1, -3, -2, -2, 5, 8],
    [-10, -4, -4, -5, -5, 5, 4, -7, -9, -4],
    [7, -10, -1, 7, -1, -5, -5, 3, 4, 6],
    [1, -5, 10, 9, 3, 9, -2, -1, 2, 8],
    [7, 10, -2, 8, 4, 1, 6, 4, -6, 4],
    [10, 4, 4, 6, 3, 8, -9, -5, 4, -2],
    [-8, -10, 10, 4, -8, -6, 3, 5, -6, 1],
    [-9, -2, -10, -6, 8, 8, 1, 6, 4, -9],
    [3, 4, -4, -3, -5, -2, -9, 1, -10, 4],
    [-5, 5, -8, -7, 6, 2, 1, 10, -10, -6],


 ])
def test_matrix_b(initial):
    a = np.zeros((10,10))

    a[0,0] = 4

    for i in xrange(1, 10):
        a[i,i] = 4
        a[i-1, i] = -1
        a[i, i-1] = -1

    # eigenvalues: 1, 2, 3, 4, 5

    value, vector = power_iteration(a, initial)
    expected_value = 5.91898595

    assert_almost_equal(value, expected_value)
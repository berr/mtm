from least_squares import least_squares
import numpy as np
from matplotlib import pyplot as plt



def show_least_squares(x, y, degree):
    plt.figure()

    x_min = np.min(x) - 1
    x_max = np.max(x) + 1
    y_min = np.min(y) - 1
    y_max = np.max(y) + 1

    plt.plot(x, y, 'bo')
    plt.xlim(x_min, x_max)
    plt.ylim(y_min, y_max)

    approximation, error = least_squares(x, y, degree)
    approximation_derivative = (np.arange(degree, -1, -1) * approximation)[0:-1]

    minimum_x = x[0]
    maximum_x = x[-1]
    roots = [r for r in np.roots(approximation_derivative) if minimum_x <= r <= maximum_x]
    roots.extend([minimum_x, maximum_x])

    f_roots = np.polyval(approximation, roots)
    max_index = np.argmax(f_roots)
    maximum_value = (roots[max_index], f_roots[max_index])


    def polynomial_to_string(p):
        p = list(reversed(p))

        s = ''
        for i, c in enumerate(p):
            if c > 0:
                s += '+'
            elif c < 0:
                s += '-'
            else:
                continue

            s += '{:.2g} '.format(np.abs(c))
            p_degree = len(p) - i - 1
            if p_degree == 0:
                continue
            elif p_degree == 1:
                s += 'x'
            else:
                s += 'x^{:d}'.format(p_degree)

        if s[0] == '+':
            s = s[1:]

        return s


    p_domain = np.linspace(x_min, x_max)
    p_image = np.polyval(approximation, p_domain)

    plot_legend = "$p_{}(x) = {}$\n $Error: {:.5g}$\n $Maximum: ({:.5g}, {:.5g})$".format(
        degree, polynomial_to_string(approximation), error, *maximum_value
    )

    plt.plot(p_domain, p_image, 'k--', label=plot_legend)
    plt.legend(loc='lower center')




x = [0, 30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330, 360]
y = [58.16, 59.02, 60.21, 60.82, 61.69, 63.31, 62.43, 63.13, 63.27, 63.70, 64.34, 62.90, 63.49]



for i in range(1, 4):
    show_least_squares(x, y, degree=i)

plt.show()


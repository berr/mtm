from lu_factorization import LU
import numpy as np


dtype_to_use = np.float32

a = np.matrix([[0.003, 59.14], [5.291, -6.13]], dtype=dtype_to_use)
b = np.matrix([[59.17], [46.78]], dtype=dtype_to_use)

lu_without_pivoting = LU.create_lu(a, partial_pivoting=False)
lu_pivoting = LU.create_lu(a, partial_pivoting=True)

x_without_pivoting = np.matrix(lu_without_pivoting.solve(b).reshape(2,1), dtype=dtype_to_use)
x_pivoting = np.matrix(lu_pivoting.solve(b).reshape(2,1), dtype=dtype_to_use)

print 'With pivoting:'
print 'x:', x_pivoting
print 'Ax - b:', (a * x_pivoting) - b

print '\n Without pivoting'
print 'x:', x_without_pivoting
print 'Ax - b:', (a * x_without_pivoting) - b


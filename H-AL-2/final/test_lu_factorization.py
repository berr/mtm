import numpy as np
import pytest
from lu_factorization import lu_factorization, LU



def test_identity():
    a = [[1, 0], [0, 1]]
    p,l,u = lu_factorization(a)


    assert np.all(p == [[1, 0], [0, 1]])
    assert np.all(l == [[1, 0], [0, 1]])
    assert np.all(u == [[1, 0], [0, 1]])


def test_no_permutation():
    a = np.matrix([[8, 2, 3], [5, 5, 1], [6, 1, 4]])
    p,l,u = lu_factorization(a)

    assert np.all(p == np.identity(3))
    assert np.all(np.abs(p * a - l * u) < 1e-7)


def test_with_permutation():
    a = np.matrix([[5, 5, 1], [6, 1, 4], [8, 2, 3]])
    p,l,u = lu_factorization(a)


    assert np.all(p == [[0, 0, 1], [1, 0, 0], [0, 1, 0]])
    assert np.all(np.abs(p * a - l * u) < 1e-7)


def test_solve_no_permutation():
    a = [[5, 2, 3, 4], [4, 9, 4, 1], [2, 0, 6, 3], [3, 4, 1, 6]]
    b = [34, 38, 32, 38]

    lu = LU.create_lu(a)
    x = lu.solve(b)
    expected_x = np.array([1, 2, 3, 4])

    assert np.all(np.abs(x - expected_x) < 1e-7)


def test_solve_with_permutation():
    a = [[4, 9, 4, 1], [5, 2, 3, 4], [3, 4, 1, 6], [2, 0, 6, 3]]
    b = [38, 34, 38, 32]

    lu = LU.create_lu(a)
    x = lu.solve(b)
    expected_x = np.array([1, 2, 3, 4])

    assert np.all(np.abs(x - expected_x) < 1e-7)


def test_singular_matrix():
    a = [[1, 1, 1], [2, 3, 4], [6, 6, 6]]

    with pytest.raises(RuntimeError) as exception_info:
        LU.create_lu(a)

    assert "Singular matrix" in str(exception_info.value)


def test_singular_only_without_partial_pivoting():
    a = [[1, 1, 1], [2, 2, 4], [5, 6, 7]]

    with pytest.raises(RuntimeError) as exception_info:
        LU.create_lu(a, partial_pivoting=False)

    assert "Singular matrix" in str(exception_info.value)

    LU.create_lu(a, partial_pivoting=True)


def test_without_partial_pivoting_solving():
    a = [[0.003, 59.14], [5.291, -6.13]]
    b = [59.17, 46.78]

    lu_without_pivoting = LU.create_lu(a, partial_pivoting=False)
    lu_pivoting = LU.create_lu(a, partial_pivoting=True)

    x_without_pivoting = lu_without_pivoting.solve(b)
    x_pivoting = lu_pivoting.solve(b)

    assert np.all(np.abs(x_without_pivoting - x_pivoting) < 1e-7)


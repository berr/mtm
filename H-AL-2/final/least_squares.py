import numpy as np
from lu_factorization import invert_upper_triangular
from qr_decomposition import qr_decomposition


def least_squares(x, y, degree=1):
    '''

    Returns the coefficients for the polynomial of degree `degree` that has the minimum sum of squared errors for
    the given points.

    Since the points x0, x1, .. xn are all different we can construct the matrix:
        [1   x0  (x0)^2  ... (x0)^n]
        [1   x1  (x1)^2  ... (x1)^n]
    A = [1   x2  (x2)^2  ... (x2)^n]
        [            ...           ]
        [1   xn  (xn)^2  ... (xn)^n]

    The A matrix has columns that are clearly linear independent if xi != xj, so we can use QR Decomposition.

    Writing the normal equations for the x points:

    A^t A y = A^t b

    But since A = QR:

    (QR)^t QR y = (QR)^t b
    R^t Q^t Q R y = R^t Q^t b, but Q is orthonormal, so Q^t Q = I

    R^t R y = R^t Q^t b. R is a upper triangular matrix, so it is invertible.

    y = (R^t R)^-1 R^t Q^t b = R^-1 (R^t)^-1 R^t Q^t b = R^-1 Q^t b

    Finally, the polynomial with minimum error for the given points has coefficients: y = R^-1 Q^t b


    :returns:
        (coefficients, error)
        Where `coefficients` is a 1-dimensional array of degree+1 entries that represent the described polynomial.
        The polynomial is stored as p(x) = p[0]x^n + p[1]x^n-1 + ... + p[2]x^2 + p[1]x + p[n].

        `error` is || Pr - y ||.
            Where Pr is coefficients projection into A's column space and y are the given observed values.
    '''
    a = np.ones((len(x), degree + 1))
    for i in xrange(1, degree + 1):
        a[:,i] = np.multiply(a[:, i- 1], x)

    a = np.asmatrix(a)
    b = np.asmatrix(y).reshape((-1, 1))

    q, r = qr_decomposition(a)
    r_inv = invert_upper_triangular(r, overwrite=True)


    result = r_inv * (q.transpose() * b)
    error = np.linalg.norm(q * (q.transpose() * b) - b)

    result = result.getA1()[::-1]

    return result, error

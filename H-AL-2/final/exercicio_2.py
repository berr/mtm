import numpy as np
from numpy import sqrt
from qr_decomposition import qr_decomposition

a = np.matrix([[1, 2], [1, 1], [2, 3], [2, 2]])
q, r = qr_decomposition(a)

print 'Q:', q
print 'R:', r


expected_q = np.matrix([
    [1.0/sqrt(10), 7.0/sqrt(110)],
    [1.0/sqrt(10), -3.0/sqrt(110)],
    [2.0/sqrt(10), 4.0/sqrt(110)],
    [2.0/sqrt(10), -6.0/sqrt(110)],
])

expected_r = np.matrix([
    [sqrt(10), 13.0/sqrt(10)],
    [0, sqrt(11.0/10.0)],
])

print 'Q erro: ', q - expected_q
print 'R erro: ', r - expected_r
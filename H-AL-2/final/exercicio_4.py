import numpy as np
from power_iteration import power_iteration


def create_a():
    a = np.zeros((5,5))

    a[0,0] = 3
    a[0,1] = 0.5
    a[0,3] = -0.5
    a[0,4] = -np.sqrt(2)

    a[1,1] = 3
    a[1,2] = 1.5

    a[2,2] = 3
    a[2,3] = -1.0/6
    a[2,4] = -np.sqrt(2)/3

    a[3,3] = 3

    a[4,4] = 3

    for i in xrange(1, 5):
        for j in xrange(i):
            a[i,j] = a[j,i]


    return a

def create_b():

    b = np.zeros((10,10))

    b[0,0] = 4

    for i in xrange(1, 10):
        b[i,i] = 4
        b[i-1, i] = -1
        b[i, i-1] = -1

    return b


def run_iterations(m, initials, expected_value, expected_vector):
    for initial in initials:
        converged = True
        try:
            obtained_value, obtained_vector = power_iteration(m, initial, max_iterations=2000)
        except:
            converged = False

        if not converged:
            print '========Diverged========'
            print tuple([round(x, 5) for x in initial])

        elif np.abs(obtained_value - expected_value) > 0.1:
            print '========Different==========='
            print initial
            print obtained_value
            print tuple([round(x, 5) for x in obtained_vector.getA1()])
            print '\n\n'


# a = create_a()
# a_values, a_vectors = np.linalg.eigh(a)
# print 'autovalores A:', a_values
# print 'autovetores A:'
# for v in a_vectors: print [round(x, 5) for x in v]
#
#
# a_initial_vectors = [
#     (1,1,1,1,1),
#     (1,0,0,0,0),
#     (0,1,0,0,0),
#     (0,0,1,0,0),
#     (0,0,0,1,0),
#     (0,0,0,0,1),
#     (-0.66609, -7.74848, 0.6529, -8.76464, 4.38032),
#     (-0.27916, -2.26901, -7.46534, -1.78768, -5.7025),
#     (5.33245, -5.18307, 4.89174, -0.91425, -2.22149),
#     (7.80315, 8.92301, 5.7606, 8.19334, -4.03032),
#     (6.86449, -4.94624, -2.57731, 6.4527, 2.33702),
#     (7.3369, 0.8943, -6.65142, -0.22865, -4.53299),
#     (1.12396, 8.2444, -0.70216, 7.60114, -1.42314),
#     (8.34178, -3.37061, 3.26872, -5.40424, 3.10007),
#     (-5.39415, 4.18613, 3.25916, -9.11183, 8.45462),
#     (-5.71208, 0.94975, -3.83073, 6.9543, -1.21136),
# ]


#for i in range(10):
#    print tuple([round(x, 5) for x in (np.random.rand(5) - 0.5) * 20])

# run_iterations(a, a_initial_vectors, a_values[-1], a_vectors[-1])



b = create_b()
b_values, b_vectors = np.linalg.eigh(b)
print 'autovalores B:', b_values
print 'autovetores B:'
for v in b_vectors: print [round(x, 5) for x in v]
print '\n\n'

b_initial_vectors = [
    (1, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    (0, 1, 0, 0, 0, 0, 0, 0, 0, 0),
    (0, 0, 1, 0, 0, 0, 0, 0, 0, 0),
    (0, 0, 0, 1, 0, 0, 0, 0, 0, 0),
    (0, 0, 0, 0, 1, 0, 0, 0, 0, 0),
    (0, 0, 0, 0, 0, 1, 0, 0, 0, 0),
    (0, 0, 0, 0, 0, 0, 1, 0, 0, 0),
    (0, 0, 0, 0, 0, 0, 0, 1, 0, 0),
    (0, 0, 0, 0, 0, 0, 0, 0, 1, 0),
    (0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
    (1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
    [ 4.0228579 , -4.71035749, -2.6143911 ,  4.39343474,  4.06131145, 7.65725395,  2.48097049, -4.91640843, -7.73717906,  3.17836018],
    [ 0.89371339,  1.20396448,  0.71488503,  0.65682686,  1.37342319, 0.62657681,  1.34317314,  0.71488503,  0.79603552,  0.89371339],
    [-0.046595  ,  0.089416  , -0.12499157,  0.8495563 ,  0.16370502,
       -0.16370502,  0.1504437 , -0.12499157, -0.089416  , -0.046595  ],
]

for i in range(1000):
    random_initials = (np.random.rand(10) - 0.5) * 20
    b_initial_vectors.append(random_initials)
    #print tuple([round(x, 5) for x in random_initials])

run_iterations(b, b_initial_vectors, b_values[-1], b_vectors[-1])
from linear_transformations import rotation, projection, reflection
import matplotlib.pyplot as plt
import numpy as np


def show_transformations(v, degrees, line):
    v = np.matrix(v, dtype=np.float64).reshape(2, 1)
    v_array = v.getA().reshape(2)
    line = np.matrix(line, dtype=np.float64).reshape(2, 1)

    r = rotation(degrees, 'deg')
    rotated = r * v
    rotated = rotated.getA().reshape(2)

    p = projection(line)
    projected = p * v
    projected = projected.getA().reshape(2)

    h = reflection(line)
    reflected = h * v
    reflected = reflected.getA().reshape(2)

    all_values = [v_array, rotated, projected, reflected]
    maximum = np.max(np.abs(all_values)) + 1
    minimum = - maximum

    def vector_data(vector):
        return np.linspace(0, vector[0]), np.linspace(0, vector[1])

    def line_data(direction):
        domain = np.arange(-10, 11)
        v = domain * float(direction[0]),  domain * float(direction[1])
        return v



    plt.figure(1)
    plt.subplot(2,2,1)
    plt.xlim(minimum, maximum)
    plt.ylim(minimum, maximum)
    plt.plot(*vector_data(v_array), color='k', label='v')
    plt.plot(*vector_data(rotated), color='r', label='Rotation')
    plt.grid()
    plt.legend(loc='best')


    plt.subplot(2,2,2)
    plt.xlim(minimum, maximum)
    plt.ylim(minimum, maximum)
    plt.plot(*vector_data(v_array), color='k', label='v')
    plt.plot(*line_data(line), color='r', linestyle='--', label='Line')
    plt.plot(*vector_data(projected), color='g', label='Projection')
    plt.grid()
    plt.legend(loc='best')


    plt.subplot(2,2,3)
    plt.xlim(minimum, maximum)
    plt.ylim(minimum, maximum)
    plt.plot(*vector_data(v_array), color='k', label='v')
    plt.plot(*line_data(line), color='r', linestyle='--', label='Line')
    plt.plot(*vector_data(reflected), color='g', label='Reflection')
    plt.grid()
    plt.legend(loc='best')

    plt.show()


show_transformations([1,1], 30, [2,3])



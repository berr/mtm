function [matriz_rotacao, matriz_projecao, matriz_reflexao] = transformacao(v, theta)
  
  hold off
  close all
  
  matriz_rotacao = [cos(theta), -sin(theta); sin(theta), cos(theta)];
  matriz_projecao = [cos(theta)^2, +cos(theta)*sin(theta); sin(theta)*cos(theta), sin(theta)^2];
  matriz_reflexao = 2 * matriz_projecao - [1, 0; 0, 1];
  
  m = max(abs(v)) + 2;
  
  rotacao = matriz_rotacao * v;
  projecao = matriz_projecao * v; 
  reflexao = matriz_reflexao * v;
  
  grid on
  hold on
  axis([-m, m, -m, m]);
  
  I = linspace(-100, 100);
  plot(I, I, 'k--')    
  plot([0 v(1)], [0, v(2)], 'g');
  
  plot([0 rotacao(1)], [0 rotacao(2)], 'y');
  plot([0 projecao(1)], [0 projecao(2)], 'b');
  plot([0 reflexao(1)], [0 reflexao(2)], 'r');    
  legend('f(x)=x', 'v', 'rotacao', 'projecao', 'reflexao');
 
 
  
end
import numpy as np


def rotation(theta, unit='radians'):
    if unit in ['degrees', 'deg']:
        theta = np.deg2rad(theta)
    else:
        assert unit in ['radians', 'rad']


    return np.matrix([[np.cos(theta), -np.sin(theta)], [np.sin(theta), np.cos(theta)]], dtype=np.float64)


def projection(projected):
    pt = np.matrix(projected, dtype=np.float64).reshape((1, -1))
    p = pt.transpose()

    return p * pt / (pt * p)


def reflection(direction):
    p = projection(direction)
    return (2 * p) - np.identity(p.shape[0])
import pytest
import numpy as np
from numpy.testing import assert_array_almost_equal
from linear_transformations import rotation, projection, reflection


@pytest.mark.parametrize('degrees, x, expected', [
    (0, [1, 0], [1, 0]),
    (45, [1, 0], [0.70710678118654757, 0.70710678118654757]),
    (-30, [0, 1], [0.5, 0.8660254037844386])

])
def test_rotation(degrees, x, expected):
    r = rotation(degrees, 'deg')
    x = np.matrix(x, dtype=np.float64).reshape(2, 1)

    rotated = r * x
    rotated = rotated.getA().reshape(2)

    assert_array_almost_equal(rotated, expected)


@pytest.mark.parametrize('x, projection_direction, expected', [
    ([10, 10], [1, 0], [10, 0]),
    ([10, 0], [1, 0], [10, 0]),
    ([-2, 0], [0, 1], [0, 0]),
    ([0, -2], [0, 1], [0, -2]),
    ([2, 2], [10, 10], [2, 2]),
])
def test_projection(x, projection_direction, expected):
    p = projection(projection_direction)
    x = np.matrix(x, dtype=np.float64).reshape(2, 1)


    projected = p * x
    projected = projected.getA().reshape(2)

    assert_array_almost_equal(projected, expected)


@pytest.mark.parametrize('x, reflection_direction, expected', [
    ([2, 3], [1, 1], [3, 2]),
    ([2, 2], [1, 1], [2, 2]),
    ([1, 0], [0, 1], [-1, 0]),
    ([-1, 1], [1, 1], [1, -1]),
])
def test_reflection(x, reflection_direction, expected):
    h = reflection(reflection_direction)
    x = np.matrix(x, dtype=np.float64).reshape(2, 1)


    reflected = h * x
    reflected = reflected.getA().reshape(2)

    assert_array_almost_equal(reflected, expected)


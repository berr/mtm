function teste_decomposicao
  
  certos = 0;
  errados = 0;
  for i=1:100
    a = round(rand(10,10) * 20 -10);
    [l u p] = decomposicao_lu(a);    
    if any(any(abs(p*a - l*u) > 0.001))
      p
      a
      u      
      l * u
      errados = errados + 1;
    else
      certos = certos + 1;
    end
  end
  
  certos
  errados
  
end
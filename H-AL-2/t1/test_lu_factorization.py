import numpy as np
from lu_factorization import lu_factorization, LU


def test_identity():
    a = [[1, 0], [0, 1]]
    p,l,u = lu_factorization(a)


    assert np.all(p == [[1, 0], [0, 1]])
    assert np.all(l == [[1, 0], [0, 1]])
    assert np.all(u == [[1, 0], [0, 1]])


def test_no_permutation():
    a = np.matrix([[8, 2, 3], [5, 5, 1], [6, 1, 4]])
    p,l,u = lu_factorization(a)

    assert np.all(p == np.identity(3))
    assert np.all(np.abs(p * a - l * u) < 1e-7)


def test_with_permutation():
    a = np.matrix([[5, 5, 1], [6, 1, 4], [8, 2, 3]])
    p,l,u = lu_factorization(a)


    assert np.all(p == [[0, 0, 1], [1, 0, 0], [0, 1, 0]])
    assert np.all(np.abs(p * a - l * u) < 1e-7)


def test_solve_no_permutation():
    a = [[5, 2, 3, 4], [4, 9, 4, 1], [2, 0, 6, 3], [3, 4, 1, 6]]
    b = [34, 38, 32, 38]

    lu = LU.create_lu(a)
    x = lu.solve(b)
    expected_x = np.array([1, 2, 3, 4])

    assert np.all(np.abs(x - expected_x) < 1e-7)


def test_solve_with_permutation():
    a = [[4, 9, 4, 1], [5, 2, 3, 4], [3, 4, 1, 6], [2, 0, 6, 3]]
    b = [34, 38, 32, 38]

    lu = LU.create_lu(a)
    x = lu.solve(b)
    expected_x = np.array([1, 2, 3, 4])

    assert np.all(np.abs(x - expected_x) < 1e-7)



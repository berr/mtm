import numpy as np


def epa_u(a):
    '''
    Factors the A matrix into 3 matrices:
    P: A permutation vector
    E: A lower triangular matrix
    U: A upper triangular matrix

    Where the following equation holds: EPA = U

    :returns (p,e,u)
    '''
    if not isinstance(a, np.matrix) or a.dtype != np.float64:
        a = np.matrix(a, dtype=np.float64, copy=False)

    n = a.shape[0]
    if not all((size == n for size in a.shape)):
        raise RuntimeError('Not a square matrix')

    minimum_value = np.MachAr().epsilon


    upper = a.copy()
    elementary = np.matrix(np.identity(n), copy=False)
    p = np.arange(n)

    pivot_column = 0

    for pivot_line in xrange(n-1):

        for i in xrange(pivot_column, n):
            new_pivot_line_offset = np.argmax(np.abs(upper[pivot_line:, i]))
            new_pivot_line = pivot_line + new_pivot_line_offset
            new_pivot = upper[new_pivot_line, pivot_column]

            if abs(new_pivot) <= minimum_value:
                continue

            if new_pivot_line != pivot_line:
                # swap lines
                upper[[pivot_line, new_pivot_line], :] = upper[[new_pivot_line, pivot_line], :]
                p[[pivot_line, new_pivot_line]] = p[[new_pivot_line, pivot_line]]
                elementary[[pivot_line, new_pivot_line], 0:pivot_line] = elementary[[new_pivot_line, pivot_line], 0:pivot_line]


            break

        else:
            raise RuntimeError("Singular matrix")


        for line_to_eliminate in xrange(pivot_line + 1, n):
            multiplier = - upper[line_to_eliminate, pivot_column] / new_pivot
            upper[line_to_eliminate, pivot_column:] += multiplier * upper[pivot_line, pivot_column:]
            elementary[line_to_eliminate, :pivot_column + 1] += multiplier * elementary[pivot_line, :pivot_column + 1]


        pivot_column += 1


    return p, elementary, upper


def p_to_permutation_matrix(p):
    size = [len(p)] * 2
    permutation = np.matrix(np.zeros(size), copy=False)

    for line, column in enumerate(p):
        permutation[line, column] = 1

    return permutation


def invert_lower_triangular(m, overwrite=False):
    if overwrite:
        inv = m
    else:
        inv = m.copy()

    for line in xrange(0, m.shape[0]):
        for column in xrange(0, line):
            s = inv[line, :line] * inv[:line, column]
            # np.inner(inv[line, :column+1], inv[:column, column+1]) should be zero, then
            # inv[line,column+1] * inv[column+1, column] = -s
            # so:
            inv[line, column] = -s / inv[line, line]


        inv[line, line] = 1 / inv[line,line]


    return inv


def invert_upper_triangular(m, overwrite=False):
    if overwrite:
        inv = m
    else:
        inv = m.copy()

    for line in xrange(m.shape[0] - 1, -1, -1):
        for column in xrange(m.shape[0] -1, line, -1):
            s = inv[line, line+1:] * inv[line+1:, column] # inner product
            v = -s / inv[line, line]
            inv[line, column] = v


        inv[line, line] = 1 / inv[line,line]


    return inv


def lu_factorization(a):
    '''
    Returns the LU factorization of A: PA = LU.
    Where:
     P is a permutation matrix
     L is a lower triangular matrix
     U is a upper triangular matrix
    '''
    p,e,u = epa_u(a)
    return p_to_permutation_matrix(p), invert_lower_triangular(e, overwrite=True), u


class LU(object):

    def __init__(self, p, l , u):
        self.p = p
        self.l = l
        self.u = u


    def solve(self, b):
        '''
        PAx = LUx = b
        x = U^-1 L^-1 b

        '''
        b = np.matrix(b, copy=False).reshape((self.l.shape[0], 1))

        # Inefficient product: Should use (sparse) triangular matrices and have a special product
        x = self.u * (self.l * b)

        return x.getA().reshape(-1)


    @classmethod
    def create_lu(cls, a):
        p,l,u = epa_u(a)
        return LU(p, l, invert_upper_triangular(u, overwrite=False))
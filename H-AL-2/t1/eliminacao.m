function m = eliminacao (x)

  m = x;
  n_colunas = size(x,2);
  n_linhas = size(x,1);

  maximo_de_iteracoes = min([n_colunas n_linhas]);
  coluna_pivo = 1;
  
  for linha_pivo=1:maximo_de_iteracoes
    
    if coluna_pivo > n_colunas
      m(linha_pivo,:) = 0
      continue
    end
  
    novo_pivo = 0;
    
    
    for i=coluna_pivo:n_colunas
      [abs_novo_pivo offset_linha_novo_pivo] = max(abs(m(linha_pivo:n_linhas,i)));
      
      if abs_novo_pivo > eps
        linha_novo_pivo = linha_pivo + offset_linha_novo_pivo - 1;
        m([linha_pivo, linha_novo_pivo], :) = m([linha_novo_pivo, linha_pivo], :);
        coluna_pivo = i;
        pivo = m(linha_pivo, i);
        break
      end
    
    end
    
    if pivo == 0
      return
    end

    
    m(linha_pivo,:) = m(linha_pivo,:) / pivo;
    
    for linha_pra_zerar=linha_pivo+1:n_linhas
      m(linha_pra_zerar, :) = m(linha_pra_zerar, :) - m(linha_pra_zerar,coluna_pivo) * m(linha_pivo,:);
    end
            
        
    coluna_pivo = coluna_pivo + 1;
        
  end
    
end



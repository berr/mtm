function [l u p] = decomposicao_lu (x)
  n = size(x, 1);  
  if size(x, 1) != size(x,2)
    error("Matriz nao quadrada\n");  
  end

  u = x;
  p = eye(n);
  l = eye(n);
  
  coluna_pivo = 1;
  
  for linha_pivo=1:n-1
  
    pivo = 0;
        
    for i=coluna_pivo:n
      [abs_novo_pivo offset_linha_novo_pivo] = max(abs(u(linha_pivo:n,i)));
      
      if abs_novo_pivo > eps
        linha_novo_pivo = linha_pivo + offset_linha_novo_pivo - 1;
        coluna_pivo = i;
        pivo = u(linha_novo_pivo, coluna_pivo);
        
        u([linha_pivo, linha_novo_pivo], :) = u([linha_novo_pivo, linha_pivo], :);
        p([linha_pivo, linha_novo_pivo], :) = p([linha_novo_pivo, linha_pivo], :);
        l([linha_pivo, linha_novo_pivo], 1:linha_pivo-1) = l([linha_novo_pivo, linha_pivo], 1:linha_pivo-1);                
        break
      end
    
    end
    
    if pivo == 0
      error("Nao inversivel")
    end


    for linha_pra_zerar=linha_pivo+1:n
      multiplicador = - u(linha_pra_zerar,coluna_pivo) / pivo;
      u(linha_pra_zerar, :) = u(linha_pra_zerar, :) + multiplicador * u(linha_pivo,:);
      l(linha_pra_zerar, :) = l(linha_pra_zerar, :) + multiplicador * l(linha_pivo,:);
    end    
        
    coluna_pivo = coluna_pivo + 1;
        
  end

  
  
  # l = inv(l)
  for i=1:n         
        
    for j=1:i-1
      soma = 0;
      
      for k=1:i-1    
        soma = soma + l(i,k) * l(k,j);
      end
      
      l(i,j) = -soma / l(i,i);
      
      
    end
    
    l(i,i) = 1 / l(i,i);
    
    
  end      
  
end
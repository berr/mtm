# teorema: circulos de gershgorin (?)

# seja A(nxn). entao existem n circulos de centro em aii
# e raio
# ri = sum |aij| (j!=i)

# contendo os autovalores de A

# gerar A com circulos disjuntos

n = 25;
m = rand(n,n) + 1i * rand(n,n);

r = sum(abs(m),2) - abs(diag(m));

d = eig(m);
plot(real(d), imag(d), 'r*', 'markersize', 10);

hold on;
plot(real(diag(m)), imag(diag(m)), 'bo', 'markersize', 10);

for i=1:n
  theta = linspace(0, 2*pi, 100);
  x = m(i,i) + r(i) * cos(theta);
  y = r(i) * sin(theta);
  plot(x,y, 'k-', 'linewidth', 1);
  hold on;
end

axis square
hold off;

# gerar matriz aleatoria com autovalores ortonormais

n = 5;

s = rand(n);
while abs(det(s)) < eps
  s = rand(n)
end


[q, r] = qr(s);

valores = rand(n,1);
lambda = diag(valores);

q * lambda * q'

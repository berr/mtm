# teorema: circulos de gershgorin (?)

#seja A(nxn). entao existem n circulos de centro em aii
# e raio
# ri = sum |aij| (j!=i)

#contendo os autovalores de A

#caso 1: m simetrica (autovalores reais)

n = 25;
m = rand(n,n);
m = m * m'; 


r = sum(abs(m),2) - abs(diag(m));

d = eig(m);
plot(d, 0*d, 'r*', 'markersize', 10);

hold on;
plot(diag(m), zeros(n), 'bo', 'markersize', 10);

for i=1:n
  theta = linspace(0, 2*pi, 100);
  x = m(i,i) + r(i) * cos(theta);
  y = r(i) * sin(theta);
  plot(x,y, 'k-', 'linewidth', 1);
  hold on;
end

axis square
hold off;

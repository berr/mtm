# 1 gerar matriz aleatoria com autovalores reais e diagonalizavel

n = 10;

s = rand(n);
while abs(det(s)) < eps
  s = rand(n)
end

lambda = diag(rand(n,1));

s * lambda * inv(s)
# gerar matriz aleatoria com autovalores distintos

n = 5;

s = rand(n);
while abs(det(s)) < eps
  s = rand(n)
end


valores = rand(n,1) + 1i * rand(n,1);
while size(unique(valores)) != size(valores)
    valores = rand(n,1) + 1i * rand(n,1);
end

lambda = diag(valores);

s * lambda * inv(s)
# gerar matriz aleatoria com autovalores quaisquer e diagonalizavel

n = 5;

s = rand(n);
while abs(det(s)) < eps
  s = rand(n)
end

lambda = diag(rand(n,1) + 1i * rand(n,1));

s * lambda * inv(s)